# coding: utf-8
# Usage:
#  ruby src/02_03_wikipedia_search.rb el < data/02_occurence_greek.csv > data/03_wikipedia_links_greek.csv

require 'open-uri'
require 'uri'
require 'json'

if ARGV[0] != 'el' && ARGV[0] != 'en' then
  exit
end
language = ARGV[0]  
STDIN.each_line.to_a[1..-1].each do |line|
  splitted_line = line.split(',')
  occurence = splitted_line[0].to_i
  street_name = splitted_line[1].strip
  data = URI.parse("https://#{language}.wikipedia.org/w/api.php?action=opensearch&search=#{URI.encode(street_name)}&format=json").read
  link_found = false
  urls = []
  JSON.parse(data).each do |result|
    if !result[0].nil? && (result[0] =~ URI::regexp) == 0 then
      urls << result
      link_found = true
      next
    end
  end
  if link_found then
    puts "#{street_name}|#{urls.join(' , ')}"
  else
    puts "#{street_name}|resultnotfound"
  end
end

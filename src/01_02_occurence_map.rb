# coding: utf-8
# Usage:
#  ruby src/01_02_occurence_map.rb orgmode < data/01_english.csv > 02_occurence_english.org
#  ruby src/01_02_occurence_map.rb csv < data/01_greek.csv > 02_occurence_greek.csv

def print_orgmode street_name_count
  puts "|Position|Road Name|Occurence|"
  puts "|--------+---------+---------|"
  
  street_name_count.sort_by {|_key, value| -value }.each_with_index do |(key, value), index|
    puts "|#{index+1}|#{key}|#{value}|"
  end
end

def print_csv street_name_count
  puts "occurence,name"
  street_name_count.sort_by {|_key, value| -value }.each_with_index do |(name, occurence), index|
    puts "#{occurence},#{name}"
  end
end

all_streets_names = STDIN.each_line.to_a
# The stopwords have been defined by observation
# https://en.wikipedia.org/wiki/Stop_words
stop_words = ["Λεωφόρος", "Leoforos", "Δρόμος", "Dromos", "Πλατεία", "Plateia", "Πάροδος", "Parodos", "1", "2", "3", "4"]
street_name_count = Hash.new(0)
all_streets_names.each do |street_name|
  words = street_name.split
  words.delete_if { |word|
    stop_words.include?(word)
  }
  new_street_name = words.join(' ')
  street_name_count[new_street_name] += 1
end

case ARGV[1]
when "orgmode"
  print_orgmode street_name_count
when "csv"
  print_csv street_name_count
else
  print_csv street_name_count
end

#!/bin/bash

# The second column of the raw data contains the address names in english
cut ../data/00_raw_data.csv -d, -f2 > ../data/01_english.csv

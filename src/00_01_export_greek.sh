#!/bin/bash

# The first column of the raw data contains the address names in greek, so we export them in a new file
cut ../data/00_raw_data.csv -d, -f1 > ../data/01_greek.csv
